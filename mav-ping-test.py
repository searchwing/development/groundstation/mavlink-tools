#!/usr/bin/env python3

import time
import random

from argparse import ArgumentParser
from pymavlink import mavutil

parser = ArgumentParser(description=__doc__)
parser.add_argument("connection", type=str)
parser.add_argument("-l", "--loss", default=0, type=float, help="Amount of Loss to be simulated in Percent %")
args = parser.parse_args()

connection = mavutil.mavlink_connection(args.connection)
loss = args.loss
packet_count = 0

while True:
    message = connection.recv_match(type='TIMESYNC')
    if message is None:
        continue
    elif message.get_type() == 'TIMESYNC':
        if(random.random() > loss) :
            now_ns = time.time() * 10**9
            connection.mav.timesync_send(int(now_ns), message.ts1) 