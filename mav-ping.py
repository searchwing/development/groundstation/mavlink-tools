#!/usr/bin/env python3

import time
import logging
from argparse import ArgumentParser
from pymavlink import mavutil

parser = ArgumentParser(description=__doc__)
parser.add_argument("connection", type=str)
parser.add_argument("-t", "--timeout", default=2,  type=int, help="Time to wait for a response in seconds.")
parser.add_argument("-i", "--interval", default=1, type=int, help="Wait interval seconds between sending each packet.")
parser.add_argument("-b", "--baudrate", type=int, help="The baudrate used in a serial connection.")
parser.add_argument("--rtscts", action="store_true", help="Enable RTS/CTS flow control")
args = parser.parse_args()
mavutil.set_dialect("ardupilotmega")
interval = args.interval
timeout = args.timeout
last_sent_timesync = 0
packets_sent = 0
packet_loss = 0
packets = []

current_time = time.strftime("%Y-%m-%d_%H-%M-%S")
log_filename = f"{current_time}.log"
logging.basicConfig(filename=log_filename, format='%(asctime)s - %(message)s', level=logging.INFO)

if args.baudrate != None:
    connection = mavutil.mavlink_connection(args.connection, baud=args.baudrate)
    if args.rtscts:
        connection.set_rtscts(True)
else:
    connection = mavutil.mavlink_connection(args.connection)
logging.info(args.connection)
 
while True:
    now = time.time()

    if (now - last_sent_timesync > interval):
        # send TIMESYNC/PING request
        last_sent_timesync = now
        time_ns = now * 10**9
        connection.mav.timesync_send(0, int(time_ns))
        packets.append(time_ns)
        packets_sent += 1

    message = connection.recv_match(type='TIMESYNC')
    if message is None:
        continue
    elif message.get_type() == 'TIMESYNC':
        if message.tc1 != 0 and message.ts1 in packets:
            # we received a response to our last timesync request
            packets.remove(message.ts1)
            now_ns = time.time() * 10**9
            rtt  = (now_ns - message.ts1) / 10**6
            logging.info("ST: timesync response: sysid=%u latency=%fms ToA=%u loss=%r" % (message.get_srcSystem(), rtt, now_ns/10**9, packet_loss/packets_sent))
            print(
                    "ST: timesync response: sysid=%u latency=%fms ToA=%u loss=%r" % (message.get_srcSystem(), rtt, now_ns/10**9, packet_loss/packets_sent)
                  )
    now_ns = now * 10**9
    for packet in packets:
        if now_ns - packet > timeout * 10**9:
            packets.remove(packet) 
            packet_loss += 1
