#!/usr/bin/env python3

import time
import logging
import sys

from argparse import ArgumentParser
from pymavlink import mavutil

parser = ArgumentParser(description=__doc__)
parser.add_argument("connection", type=str)
parser.add_argument("-b", "--baudrate", default=None, type=int, help="Baudrate of the serial port")
parser.add_argument("-s", "--src", default=255, type=int, help="System ID of the source we care about.")
parser.add_argument("-c", "--cmp", default=255, type=int, help="Component ID of the source we care about.")
parser.add_argument("-d", "--device", default='x', type=str, help="Version of RFD Modem e.g. x or +")
args = parser.parse_args()

current_time = time.strftime("%Y-%m-%d_%H-%M-%S")
log_filename = f"{current_time}.log"
logging.basicConfig(filename=log_filename, format='%(asctime)s - %(message)s', level=logging.INFO)

if args.baudrate is not None:
    connection = mavutil.mavlink_connection(args.connection, baud=args.baudrate)
else:
    connection = mavutil.mavlink_connection(args.connection)

count = 0
count_lost = 0
last_time = time.time_ns()
last_count = count
paket_rate = 0
last_seqno = -1
lost = ""
crc_errors = 0

noise = 0
remnoise = 0
rssi = 0
remrssi = 0
rxerrors = 0
txbuf = 0


connection.mav.heartbeat_send(
        mavutil.mavlink.MAV_TYPE_GCS,
        mavutil.mavlink.MAV_AUTOPILOT_INVALID,
        0,
        0,
        0
)

# read a few messages because the adapter/system buffers some stuff somewhere
# otherwise the paket loss starts high because we read a old paket from with an
# old sequence number
for i in range(0, 10):
    message = connection.recv_match()


while True:
    now = time.time_ns()
    message = connection.recv_match()
    time_delta = now - last_time
    if time_delta > 1000000000:
        paket_rate = (count - last_count) / (time_delta / 1000000000)
        last_time = now
        last_count = count
        connection.mav.heartbeat_send(
                mavutil.mavlink.MAV_TYPE_GCS,
                mavutil.mavlink.MAV_AUTOPILOT_INVALID,
                0,
                0,
                0
        )
    if message is None:
        continue
    elif message.get_type() == 'BAD_DATA' and message.reason.find('invalid MAVLink CRC') >= 0:
        crc_errors +=1 
        continue
    elif message.get_type() == 'RADIO_STATUS':
        noise = message.noise
        remnoise = message.remnoise
        rssi = message.rssi
        remrssi = message.remrssi
        rxerrors = message.rxerrors
        txbuf = message.txbuf
    elif message.get_srcSystem() == args.src and message.get_srcComponent() == args.cmp:
        count += 1
        print("Message size in bytes: %u" % (sys.getsizeof(message)))
        print("Message payload length: %u" % (len(message._payload)))
        seqno = message.get_seq()
        if last_seqno == -1 or (last_seqno + 1) % 256 == seqno:
            pass
        else:
            lost = "!"
            count_lost += (seqno - last_seqno - 1) % 256
        last_seqno = seqno
        def output(dbm_formula_parm1, dbm_formula_parm2):
            output = "src sys id=%u seqno=%u paket count=%u lost=%u time delta=%f \ paket/s=%f paket loss=%f rssi=%u remrssi=%u noise=%u \ remnoise=%u rxerrors=%u txbuf=%u %s crcerrors=%u" % (
                message.get_srcSystem(),
                message.get_seq(),
                count,
                count_lost,
                (time_delta / 1000000000),
                paket_rate,
                count_lost / (count+count_lost),
                rssi / dbm_formula_parm1 - dbm_formula_parm2,
                remrssi / dbm_formula_parm1 - dbm_formula_parm2,
                noise / dbm_formula_parm1 - dbm_formula_parm2,
                remnoise / dbm_formula_parm1 - dbm_formula_parm2,
                rxerrors,
                txbuf,
                lost,
                crc_errors
            )
            return output
        if args.device == '+':
            logging.info(output(1.9, 127))
            print(output(1.9, 127))
        else:
            logging.info(output(2, 152))
            print(output(2, 152))
        lost = ""
